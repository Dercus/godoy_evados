<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>

    </head>


    <body background="resources/fondoGranja.jpg">

        <div class="container">

            <div class="jumbotron">
                <br>
                <h1 class="displau-4" style="text-align: center">
                    <p><strong>Mi Granja</strong></p>
                </h1>
                <br>
                <br>
                <p class="lead" style="text-align: center">Rodrigo Godoy</p>







                <br><!-- espacio entre filas -->

                <div class="row">
                    <div class="col-md-4">

                        <div class="row">
                            <div class="col-md-4"> </div>  <!-- espacio entre columnas -->
                            <div class="col-md-4">
                                <div class="card">
                                    <img class="card-img-top" src="resources/michi.jpg">
                                    <div class="card-body">
                                        <p class="card-text">Gatito</p>

                                    </div>
                                </div>
                                <br>

                                <p class="lead" style="text-align: center">
                                <audio id="maullido" src="resources/kitty.mp3"></audio>
                                <a class="btn btn-primary btn-lg" onclick="document.getElementById('maullido').play(); alert('20 segundos de Gatitos bebes')" href="#"  role="button">Miau?</a>
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">

                        <div class="row">
                            <div class="col-md-4"> </div> 
                            <div class="col-md-4">
                                <div class="card">
                                    <img class="card-img-top" src="resources/doggo.jpg">
                                    <div class="card-body">
                                        <p class="card-text">Perro</p>
                                    </div>
                                </div>
                                <br>
                                <p class="lead" style="text-align: center">
                                    <audio id="ladrido" src="resources/perro.mp3"></audio>
                                    <a class="btn btn-primary btn-lg" onclick="document.getElementById('ladrido').play(); alert('Un Doggo!')" href="#" role="button">Woof?</a>
                                </p>
                            </div>
                        </div><!-- comment -->
                    </div>


                    <div class="col-md-4">

                        <div class="row">
                            <div class="col-md-4"> </div>  <!-- espacio entre columnas -->

                            <div class="col-md-4">
                                <div class="card">
                                    <img class="card-img-top" src="resources/cuy.jpg">
                                    <div class="card-body">
                                        <p class="card-text">Cobaya o Cuyi</p>
                                    </div>
                                </div>
                                <br>
                                <p class="lead" style="text-align: center">
                                    <audio id="cuycuy" src="resources/cuyi.wav"></audio>
                                    <a class="btn btn-primary btn-lg" onclick="document.getElementById('cuycuy').play(); alert('Un pequeño Cuyi contento...o enojado, o con hambre, honestamente no logro distinguir las emociones de estos peques')" href="#" role="button">CUY!!</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-4">
                        <div class="row">

                            <div class="col-md-4"> </div>
                            <div class="col-md-4"> </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <img class="card-img-top" src="resources/pollito.jpg">
                                    <div class="card-body">
                                        <p class="card-text">Pollito</p>
                                    </div>
                                </div>
                                <br>
                                <p class="lead" style="text-align: center">
                                    <audio id="pio" src="resources/chick.wav"></audio>
                                    <a class="btn btn-primary btn-lg" onclick="document.getElementById('pio').play(); alert('Un Pollito')" href="#" role="button">Pio?</a>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4"> </div>

                    <div class="col-md-4">
                        <div class="row">


                            <div class="col-md-4">
                                <div class="card">
                                    <img class="card-img-top" src="resources/Cow.jpg">
                                    <div class="card-body">
                                        <p class="card-text">Vaca</p>
                                    </div>
                                </div>
                                <br>
                                <p class="lead"style="text-align: center">
                                    <audio id="moo" src="resources/cow.mp3"></audio>
                                    <a class="btn btn-primary btn-lg" onclick="document.getElementById('moo').play(); alert('Esa es una vaca')" href="#" role="button">Moo?</a>

                                </p>
                            </div>

                        </div>
                    </div>

                </div>


            </div>

        </div>


    </body>

</html>
